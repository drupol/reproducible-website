---
layout: report
year: "2023"
month: "06"
title: "Reproducible Builds in June 2023"
draft: false
date: 2023-07-12 13:17:16
---

**Welcome to the June 2023 report from the [Reproducible Builds]({{ "/" | relative_url }}) project**
{: .lead}

[![]({{ "/images/reports/2023-06/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

In our reports, we outline the most important things that we have been up to over the past month. As always, if you are interested in contributing to the project, please visit our [*Contribute*]({{ "/contribute/" | relative_url }}) page on our website.

---

[![]({{ "/images/reports/2023-06/summit.jpg#right" | relative_url }})]({{ "/news/2023/07/05/reproducible-builds-hamburg-meeting/" | relative_url }})

**We are very happy to announce the [upcoming Reproducible Builds Summit]({{ "/news/2023/07/05/reproducible-builds-hamburg-meeting/" | relative_url }}) which set to take place from October 31st — November 2nd 2023, in the vibrant city of Hamburg, Germany.**

Our summits are a unique gathering that brings together attendees from diverse projects, united by a shared vision of advancing the Reproducible Builds effort. During this enriching event, participants will have the opportunity to engage in discussions, establish connections and exchange ideas to drive progress in this vital field. Our aim is to create an inclusive space that fosters collaboration, innovation and problem-solving. We are thrilled to host the seventh edition of this exciting event, following the success of previous summits in various iconic locations around the world, including Venice, Marrakesh, Paris, Berlin and Athens.

If you're interesting in joining us this year, please make sure to read the [event page]({{ "/news/2023/07/05/reproducible-builds-hamburg-meeting/" | relative_url }})] which has more details about the event and location. (You may also be interested in attending [PackagingCon 2023](https://packaging-con.org/) held a few days before in Berlin.)

---

[![]({{ "/images/reports/2023-06/fossy.png#right" | relative_url }})](https://2023.fossy.us/schedule/presentation/118/)

This month, Vagrant Cascadian will present at [FOSSY 2023](https://2023.fossy.us/) on the topic of [*Breaking the Chains of Trusting Trust*](https://2023.fossy.us/schedule/presentation/118/):

> Corrupted build environments can deliver compromised cryptographically signed binaries. Several exploits in critical supply chains have been demonstrated in recent years, proving that this is not just theoretical. The most well secured build environments are still single points of failure when they fail. […] This talk will focus on the state of the art from several angles in related Free and Open Source Software projects, what works, current challenges and future plans for building trustworthy toolchains you do not need to trust.

Hosted by the [Software Freedom Conservancy](https://sfconservancy.org/) and taking place in Portland, Oregon, FOSSY aims to be a community-focused event: "Whether you are a long time contributing member of a free software project, a recent graduate of a coding bootcamp or university, or just have an interest in the possibilities that free and open source software bring, FOSSY will have something for you". More information on the event is available [on the FOSSY 2023 website](https://2023.fossy.us/about/), including the [full programme schedule](https://2023.fossy.us/schedule/).

---

[![]({{ "/images/reports/2023-06/paper-floss.png#right" | relative_url }})](https://www.computer.org/csdl/proceedings-article/sp/2023/933600b527/1NrbYOMgAzS)

Marcel Fourné, Dominik Wermke, William Enck, Sascha Fahl and Yasemin Acar recently published an academic paper in the [44th IEEE Symposium on Security and Privacy](https://www.computer.org/csdl/proceedings-article/sp/2023/933600b527/1NrbYOMgAzS) titled "It’s like flossing your teeth: On the Importance and Challenges of Reproducible Builds for Software Supply Chain Security". The abstract reads as follows:

> The 2020 Solarwinds attack was a tipping point that caused a heightened awareness about the security of the software supply chain and in particular the large amount of trust placed in build systems. Reproducible Builds (R-Bs) provide a strong foundation to build defenses for arbitrary attacks against build systems by ensuring that given the same source code, build environment, and build instructions, bitwise-identical artifacts are created.

However, in contrast to other papers that touch on some theoretical aspect of reproducible builds, the authors' paper takes a different approach. Starting with the observation that "much of the software industry believes R-Bs are too far out of reach for most projects" and conjoining that with a goal of "to help identify a path for R-Bs to become a commonplace property", the paper has a different methodology:

> **We conducted a series of 24 semi-structured expert interviews with participants from the Reproducible-Builds.org project**, and iterated on our questions with the reproducible builds community. We identified a range of motivations that can encourage open source developers to strive for R-Bs, including indicators of quality, security benefits, and more efficient caching of artifacts. We identify experiences that help and hinder adoption, which heavily include communication with upstream projects. We conclude with recommendations on how to better integrate R-Bs with the efforts of the open source and free software community.

A [PDF of the paper](https://saschafahl.de/static/paper/reprobuilds2023.pdf) is now available, as is an [entry](https://publications.cispa.saarland/3933/) on the [CISPA Helmholtz Center for Information Security](https://cispa.de/) website and an [entry](https://publications.teamusec.de/2023-oakland-repro/) under the [TeamUSEC](https://teamusec.de/) Human-Centered Security research group.

---

On [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general/) this month:

* Martin Monperrus asked whether there are "[any project[s] where reproducibility is checked in a continuous integration pipeline?](https://lists.reproducible-builds.org/pipermail/rb-general/2023-June/003007.html) which received [a number of replies](https://lists.reproducible-builds.org/pipermail/rb-general/2023-June/thread.html#3007) from various projects.

* Vagrant Cascadian mentioned that [Packaging Con 2023](https://packaging-con.org/) is being held in Berlin, the weekend before the Reproducible Builds summit later this year. In particular, Vagrant noticed that the [Call for Proposals](https://cfp.packaging-con.org/2023/cfp) (CFP) closes at the end of July.

* Larry Doolittle was searching Usenet archives and discovered a thread from December 1999 titled "[*Time independent checksum(cksum)*](https://www.usenetarchives.com/view.php?id=comp.unix.programmer&mid=PDgyaDhsMyR2ZWYkMUBubnJwMS5kZWphLmNvbT4) on `comp.unix.programming`. Larry notes that "it starts with Jayan asking about comparing binaries that might have difference in their embedded timestamps" (that is, perhaps, "Foreshadowing diffoscope, amiright?") and goes on to observe that:

> The antagonist is David Schwartz, who correctly says "There are dozens of complex reasons why what seems to be the same sequence of operations might produce different end results," but goes on to say "I totally disagree with your general viewpoint that compilers must provide for reproducability [sic]."
>
> Dwight Tovey and I (Larry Doolittle) argue for reproducible builds. I assert "Any program -- especially a mission-critical program like a compiler -- that cannot reproduce a result at will is broken."  Also "it's commonplace to take a binary from the net, and check to see if it was trojaned by attempting to recreate it from source."

---

Lastly, there were a few changes to our website this month too, including Bernhard M. Wiedemann adding a simplified Rust example to our [documentation about the `SOURCE_DATE_EPOCH` environment variable]({{ "/docs/source-date-epoch/" | relative_url }})&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/f4b89e17)], Chris Lamb made it easier to parse our [summit announcement]({{ "/news/2023/07/05/reproducible-builds-hamburg-meeting/" | relative_url }}") at a glance&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/b0d7a607)], Mattia Rizzolo added the [summit announcement]({{ "/news/2023/07/05/reproducible-builds-hamburg-meeting/" | relative_url }}") at a glance&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/b0d7a607)] itself&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/60a4e16e)][[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/57d62ce6)][[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/811c13fa)] and Rahul Bajaj added a [taxonomy of variations in build environments]({{ "/docs/plans/" | relative_url }})&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/0dbb8ed4)].

---

## Distribution work

[![]({{ "/images/reports/2023-06/debian.png#right" | relative_url }})](https://debian.org/)

27 reviews of Debian packages were added, 40 were updated and 8 were removed this month adding to [our knowledge about identified issues](https://tests.reproducible-builds.org/debian/index_issues.html). A new `randomness_in_documentation_generated_by_mkdocs` toolchain issue was added by Chris Lamb&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/5b86ed84)], and the `deterministic` flag on the `paths_vary_due_to_usrmerge` issue as we are not currently testing [`usrmerge`](https://wiki.debian.org/UsrMerge) issues&nbsp;[[…](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/8dc078ff)] issues.

<br>

Roland Clobus posted his [18th update of the status of reproducible Debian ISO images](https://lists.reproducible-builds.org/pipermail/rb-general/2023-July/003021.html) on our mailing list. Roland reported that "all major desktops build reproducibly with `bullseye`, `bookworm`, `trixie` and `sid`", but he also mentioned amongst many changes that not only are the `non-free` images being built (and are reproducible) but that the live images are "generated officially by Debian" itself. [[...](https://lists.reproducible-builds.org/pipermail/rb-general/2023-July/003021.html)]

<br>

Jan-Benedict Glaw noticed a problem when building [NetBSD](https://www.netbsd.org/) for the [VAX](https://en.wikipedia.org/wiki/VAX) architecture. Noting that "Reproducible builds [are] probably not as reproducible as we thought", Jan-Benedict goes on to describe that when "two builds from different source directories won't produce the same result" and adds various notes about sub-optimal handling of the `CFLAGS` environment variable.&nbsp;[[…](http://gnats.netbsd.org/cgi-bin/query-pr-single.pl?number=57466)]

<br>

[![]({{ "/images/reports/2023-06/fdroid.png#right" | relative_url }})](https://f-droid.org/en/)

F-Droid added 21 new reproducible apps in June, resulting in a new record of 145 reproducible apps in total.&nbsp;[[…](https://gitlab.com/obfusk/fdroid-misc-scripts/-/blob/master/reproducible/overview.md)]. (This page now sports missing data for March—May 2023.) F-Droid contributors also reported an issue with broken resources in APKs making some builds unreproducible.&nbsp;[[…](https://issuetracker.google.com/issues/287967713)]

<br>

[![]({{ "/images/reports/2023-06/opensuse.png#right" | relative_url }})](https://www.opensuse.org/)

Bernhard M. Wiedemann published another [monthly report about reproducibility within openSUSE](https://lists.opensuse.org/archives/list/factory@lists.opensuse.org/thread/WB5NTG4Q6KK2YLIZ4OF65MI5CQDOEA3B/)

---

## Upstream patches

* Bernhard M. Wiedemann:

    * [`bcachefs`](https://github.com/koverstreet/bcachefs-tools/pull/145) (sort find / filesys)
    * [`build-compare`](https://github.com/openSUSE/build-compare/issues/64) (reports files as identical)
    * [`build-time`](https://github.com/AlephAlpha/build-time/pull/5) (toolchain date)
    * [`cockpit`](https://github.com/cockpit-project/cockpit/pull/18994) (merged, gzip mtime)
    * [`gcc13`](https://bugzilla.opensuse.org/show_bug.cgi?id=1212698) (gcc13 toolchain LTO parallelism)
    * [`ghc-rpm-macros`](https://build.opensuse.org/request/show/1095295) (toolchain parallelism)
    * [`golangcli-lint`](https://build.opensuse.org/request/show/1073618) (date)
    * [`gutenprint`](https://sourceforge.net/p/gimp-print/source/merge-requests/2/) (date+time)
    * [`mage`](https://github.com/magefile/mage/pull/474) (date (golang))
    * [`mumble`](https://github.com/mumble-voip/mumble/pull/6147) (filesys)
    * [`pcr`](https://github.com/okirch/pcr-oracle/pull/29) (date)
    * [`python-nss`](https://build.opensuse.org/request/show/1094593) (drop sphinx .doctrees)
    * [`python310`](https://bugzilla.opensuse.org/show_bug.cgi?id=1211765) (merged, bisected+backported)
    * [`warpinator`](https://github.com/linuxmint/warpinator/pull/178) (merged, date)
    * [`xroachng`](https://build.opensuse.org/request/show/1091749) (date)

* Chris Lamb:

    * [#1037159](https://bugs.debian.org/1037159) filed against [`elinks`](https://tracker.debian.org/pkg/elinks).
    * [#1037205](https://bugs.debian.org/1037205) filed against [`multipath-tools`](https://tracker.debian.org/pkg/multipath-tools).
    * [#1037216](https://bugs.debian.org/1037216) filed against [`mkdocstrings-python-handlers`](https://tracker.debian.org/pkg/mkdocstrings-python-handlers).
    * [#1038730](https://bugs.debian.org/1038730) filed against [`fribidi`](https://tracker.debian.org/pkg/fribidi).
    * [#1038957](https://bugs.debian.org/1038957) filed against [`jtreg7`](https://tracker.debian.org/pkg/jtreg7).
    * [#1039932](https://bugs.debian.org/1039932) filed against [`python-bitstring`](https://tracker.debian.org/pkg/python-bitstring) ([forwarded upstream](https://github.com/scott-griffiths/bitstring/pull/269)).

* Vagrant Cascadian:

    * [#1037235](https://bugs.debian.org/1037235) filed against [`gradle-kotlin-dsl`](https://tracker.debian.org/pkg/gradle-kotlin-dsl).
    * [#1037240](https://bugs.debian.org/1037240) filed against [`libsdl-console`](https://tracker.debian.org/pkg/libsdl-console).
    * [#1037267](https://bugs.debian.org/1037267) filed against [`kawari8`](https://tracker.debian.org/pkg/kawari8).
    * [#1037269](https://bugs.debian.org/1037269) filed against [`freetds`](https://tracker.debian.org/pkg/freetds).
    * [#1037272](https://bugs.debian.org/1037272) filed against [`gbrowse`](https://tracker.debian.org/pkg/gbrowse).
    * [#1037276](https://bugs.debian.org/1037276) filed against [`bglibs`](https://tracker.debian.org/pkg/bglibs).
    * [#1037277](https://bugs.debian.org/1037277) filed against [`advi`](https://tracker.debian.org/pkg/advi).
    * [#1037278](https://bugs.debian.org/1037278) filed against [`afterstep`](https://tracker.debian.org/pkg/afterstep).
    * [#1037280](https://bugs.debian.org/1037280) filed against [`simstring`](https://tracker.debian.org/pkg/simstring).
    * [#1037296](https://bugs.debian.org/1037296) filed against [`manderlbot`](https://tracker.debian.org/pkg/manderlbot).
    * [#1037298](https://bugs.debian.org/1037298) filed against [`erlang-proper`](https://tracker.debian.org/pkg/erlang-proper).
    * [#1037300](https://bugs.debian.org/1037300) filed against [`comedilib`](https://tracker.debian.org/pkg/comedilib).
    * [#1037309](https://bugs.debian.org/1037309) filed against [`libint`](https://tracker.debian.org/pkg/libint).
    * [#1037427](https://bugs.debian.org/1037427) filed against [`newlib`](https://tracker.debian.org/pkg/newlib).
    * [#1038908](https://bugs.debian.org/1038908) filed against [`binutils-msp430`](https://tracker.debian.org/pkg/binutils-msp430).
    * [#1038970](https://bugs.debian.org/1038970) & [#1038971](https://bugs.debian.org/1038971) filed against [`c-munipack`](https://tracker.debian.org/pkg/c-munipack).
    * [#1039044](https://bugs.debian.org/1039044) filed against [`python-marshmallow-sqlalchemy`](https://tracker.debian.org/pkg/python-marshmallow-sqlalchemy).
    * [#1039457](https://bugs.debian.org/1039457) filed against [`mplayer`](https://tracker.debian.org/pkg/mplayer).
    * [#1039493](https://bugs.debian.org/1039493) & [#1039500](https://bugs.debian.org/1039500) filed against [`menu`](https://tracker.debian.org/pkg/menu).
    * [#1039506](https://bugs.debian.org/1039506) filed against [`mini-buildd`](https://tracker.debian.org/pkg/mini-buildd).
    * [#1039610](https://bugs.debian.org/1039610) filed against [`pnetcdf`](https://tracker.debian.org/pkg/pnetcdf).
    * [#1039617](https://bugs.debian.org/1039617) & [#1039618](https://bugs.debian.org/1039618) filed against [`liblopsub`](https://tracker.debian.org/pkg/liblopsub).
    * [#1039619](https://bugs.debian.org/1039619) filed against [`wcc`](https://tracker.debian.org/pkg/wcc).
    * [#1039623](https://bugs.debian.org/1039623) filed against [`shotcut`](https://tracker.debian.org/pkg/shotcut).
    * [#1039624](https://bugs.debian.org/1039624) filed against [`icu`](https://tracker.debian.org/pkg/icu).
    * [#1039741](https://bugs.debian.org/1039741) filed against [`libapache-poi-java`](https://tracker.debian.org/pkg/libapache-poi-java).
    * [#1039808](https://bugs.debian.org/1039808) filed against [`atf`](https://tracker.debian.org/pkg/atf).
    * [#1039865](https://bugs.debian.org/1039865) filed against [`valgrind`](https://tracker.debian.org/pkg/valgrind).

---

## Testing framework

[![]({{ "/images/reports/2023-06/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

The Reproducible Builds project operates a comprehensive testing framework (available at [tests.reproducible-builds.org](https://tests.reproducible-builds.org)) in order to check packages and other artifacts for reproducibility. In June, a number of changes were made by Holger Levsen, including:

* Additions to a (relatively) new Documented Jenkins Maintenance (*djm*) script to automatically shrink a cache & save a backup of old data&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/340e50e42)], automatically split out previous months data from logfiles into specially-named files&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/2e384f30f)], prevent concurrent remote logfile fetches by using a lock file&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/dc3e58086)] and to add/remove various debugging statements&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/6bc60be7f)].

* Updates to the automated system health checks to, for example, to correctly detect new kernel warnings due to a wording change&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/610fc16a6)] and to explicitly observe which old/unused kernels should be removed&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/54e401f47)]. This was related to an improvement so that various kernel issues on Ubuntu-based nodes are automatically fixed.&nbsp;[[…](https://salsa.debian.org/qa/jenkins.debian.net/commit/a61ed3ca3)]

Holger and Vagrant Cascadian updated all thirty-five hosts running Debian on the `amd64`, `armhf`, and `i386` architectures to Debian *bookworm*, with the exception of the Jenkins host itself which will be upgraded after the release of Debian 12.1. In addition, Mattia Rizzolo updated the email configuration for the `@reproducible-builds.org` domain to correctly accept incoming mails from `jenkins.debian.net`&nbsp;[[…](https://salsa.debian.org/reproducible-builds/rb-mailx-ansible/commit/0256761)] as well as to set up [DomainKeys Identified Mail](https://en.wikipedia.org/wiki/DomainKeys_Identified_Mail) (DKIM) signing&nbsp;[[…](https://salsa.debian.org/reproducible-builds/rb-mailx-ansible/commit/249b009)]. And working together with Holger, Mattia also updated the Jenkins configuration to start testing Debian *trixie* which resulted in stopped testing Debian *buster*. And, finally, Jan-Benedict Glaw contributed patches for improved NetBSD testing.

<br>

---

If you are interested in contributing to the Reproducible Builds project, please visit our [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. However, you can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)

 * Twitter: [@ReproBuilds](https://twitter.com/ReproBuilds)
