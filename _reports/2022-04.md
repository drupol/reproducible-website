---
layout: report
year: "2022"
month: "04"
title: "Reproducible Builds in April 2022"
draft: false
date: 2022-05-05 19:18:55
---

[![]({{ "/images/reports/2022-04/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

*Welcome to the April 2022 report from the [Reproducible Builds](https://reproducible-builds.org) project!* In these reports, we try to summarise the most important things that we have been up to over the past month. If you are interested in contributing to the project, please take a few moments to visit our [*Contribute*]({{ "/contribute/" | relative_url }}) page on our website.

## News

Cory Doctorow published an interesting article this month about the possibility of [*Undetectable backdoors for machine learning models*](https://doctorow.medium.com/undetectable-backdoors-for-machine-learning-models-8df33d92da30). Given that machine learning models can provide unpredictably incorrect results, Doctorow recounts that there exists another category of "adversarial examples" that comprise "a gimmicked machine-learning input that, to the human eye, seems totally normal — but which causes the ML system to misfire dramatically" that permit the possibility of planting "undetectable back doors into any machine learning system at training time".

<br>


[![]({{ "/images/reports/2022-04/ardc.png#right" | relative_url }})]({{ "/news/2022/04/14/supporter-spotlight-ardc/" | relative_url }})
[![]({{ "/images/reports/2022-04/gosst.png#right" | relative_url }})]({{ "/news/2022/04/26/supporter-spotlight-google-open-source-security-team/" | relative_url }})

Chris Lamb published two 'supporter spotlights' on our blog: the first about [**Amateur Radio Digital Communications** (ARDC)]({{ "/news/2022/04/14/supporter-spotlight-ardc/" | relative_url }}) and the second about the [**Google Open Source Security Team** (GOSST)]({{ "/news/2022/04/26/supporter-spotlight-google-open-source-security-team/" | relative_url }}).

<br>

[![]({{ "/images/reports/2022-04/paper.png#right" | relative_url }})](https://arxiv.org/pdf/2204.04008.pdf)

Piergiorgio Ladisa, Henrik Plate, Matias Martinez and Olivier Barais published a new academic paper titled [*A Taxonomy of Attacks on Open-Source Software Supply Chains*](https://arxiv.org/pdf/2204.04008.pdf) (PDF):

> This work proposes a general taxonomy for attacks on open-source supply chains, independent of specific programming languages or ecosystems, and covering all supply chain stages
from code contributions to package distribution. Taking the form of an attack tree, it covers 107 unique vectors, linked to 94 real-world incidents, and mapped to 33 mitigating safeguards.

<br>

Elsewhere in academia, Ly Vu Duc published his PhD thesis. Titled [*Towards Understanding and Securing the OSS Supply Chain*](https://www.researchgate.net/profile/Duc-Ly-Vu/publication/359878507_Towards_Understanding_and_Securing_the_OSS_Supply_Chain/links/6254748ecf60536e2354f615/Towards-Understanding-and-Securing-the-OSS-Supply-Chain.pdf) (PDF), Duc's abstract reads as follows:

> This dissertation starts from the first link in the software supply chain, 'developers'. Since many developers do not update their vulnerable software libraries, thus exposing the user of their code to security risks. To understand how they choose, manage and update the libraries, packages, and other Open-Source Software (OSS) that become the building blocks of companies' completed products consumed by end-users, twenty-five semi-structured interviews were conducted with developers of both large and small-medium enterprises in nine countries. All interviews were transcribed, coded, and analyzed according to applied thematic analysis

<br>

## Upstream news

[![]({{ "/images/reports/2022-04/golang.png#right" | relative_url }})](https://go.dev/blog/supply-chain)

[Filippo Valsorda](https://filippo.io/) published an informative blog post recently called [*How Go Mitigates Supply Chain Attacks*](https://go.dev/blog/supply-chain) outlining the high-level features of the Go ecosystem that helps prevent various supply-chain attacks.

<br>

There was new/further activity on a [pull request filed against *openssl*](https://github.com/openssl/openssl/pull/11545) by Sebastian Andrzej Siewior in order to prevent saved `CFLAGS` (which may contain the `-fdebug-prefix-map=<PATH>` flag that is used to strip an arbitrary the build path from the debug info — if this information remains recorded then the binary is no longer reproducible if the build
directory changes.

<br>

## Events

[![]({{ "/images/reports/2022-04/supplychainsecuritycon.png#right" | relative_url }})](https://events.linuxfoundation.org/open-source-summit-north-america/about/supplychainsecuritycon/)

The [Linux Foundation](https://linuxfoundation.org)'s [SupplyChainSecurityCon](https://events.linuxfoundation.org/open-source-summit-north-america/about/supplychainsecuritycon/), will take place June 21st — 24th 2022, both virtually and in Austin, Texas. Long-time Reproducible Builds and openSUSE contributor Bernhard M. Wiedemann learned that he had his talk accepted, and will speak on [*Reproducible Builds: Unexpected Benefits and Problems*](https://ossna2022.sched.com/event/11NpJ/reproducible-builds-unexpected-benefits-and-problems-bernhard-m-wiedemann-suse) on June 21st.

<br>

[![]({{ "/images/reports/2022-04/debian-hamburg.png#right" | relative_url }})](https://wiki.debian.org/DebianEvents/de/2022/DebianReunionHamburg)

There will be an in-person "Debian Reunion" in Hamburg, Germany later this year, taking place from 23 — 30 May. Although this is a "Debian" event, there will be some folks from the broader Reproducible Builds community and, of course, everyone is welcome. Please see the [event page on the Debian wiki](https://wiki.debian.org/DebianEvents/de/2022/DebianReunionHamburg) for more information. 41 people have registered so far, and there's approx 10 "on-site" beds still left.

<br>

The [minutes and logs from our April 2022 IRC meeting](http://meetbot.debian.net/reproducible-builds/2022/reproducible-builds.2022-04-26-14.59.html) have been published. In case you missed this one, our next IRC meeting will take place on May 31st at 15:00 UTC on `#reproducible-builds` on the [OFTC](https://oftc.net) network.

<br>

## Debian

[![]({{ "/images/reports/2022-04/debian.png#right" | relative_url }})](https://debian.org/)

Roland Clobus wrote another [in-depth status update](https://lists.reproducible-builds.org/pipermail/rb-general/2022-April/002540.html) about the status of 'live' Debian images, summarising the current situation that all major desktops build reproducibly with *bullseye*, *bookworm* and *sid*, including the [*Cinnamon* desktop](https://cinnamon-spices.linuxmint.com/) on *bookworm* and *sid*, "but at a small functionality cost: 14 words will be incorrectly abbreviated". This work incorporated:

* Reporting an issue about unnecessarily modified timestamps in the daily Debian installer images. [[...](https://salsa.debian.org/installer-team/debian-installer/-/merge_requests/26)]
* Reporting a bug against the `debian-installer`: in order to use a suitable kernel version. ([#1006800](https://bugs.debian.org/1006800))
* Reporting a bug in: `texlive-binaries` regarding the unreproducible content of `.fmt` files. ([#1009196](https://bugs.debian.org/1009196))
* Adding hacks to make the Cinnamon desktop image reproducible in *bookworm* and *sid*. [[...](https://salsa.debian.org/live-team/live-build/-/merge_requests/279)]
* Added a script to rebuild a live-build ISO image from a given timestamp. [[...](https://salsa.debian.org/live-team/live-build/-/commit/a9d367d406de014f8a2f864ebda6504d45d679d)
* etc.

On [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general/), Venkata Pyla started a thread on the Debian [*debconf cache is non-reproducible issue while creating system images*](https://lists.reproducible-builds.org/pipermail/rb-general/2022-April/002546.html) and Vagrant Cascadian posted an excellent [summary of the reproducibility status of core package sets in Debian](https://lists.reproducible-builds.org/pipermail/rb-general/2022-April/002548.html) and solicited for similar information from other distributions.

<br>

Lastly, 122 reviews of Debian packages were added, 44 were updated and 193 were removed this month adding to [our extensive knowledge about identified issues](https://tests.reproducible-builds.org/debian/index_issues.html). A number of issue types have been updated as well, including [`timestamps_generated_by_hevea`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/a4cb5a35), [`randomness_in_ocaml_preprocessed_files`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/c9da98ac), [`build_path_captured_in_emacs_el_file`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/24466214), [`golang_compiler_captures_build_path_in_binary`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/63d03a65) and [`build_path_captured_in_assembly_objects`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/d2923ece),

<br>

## Other distributions

[![]({{ "/images/reports/2022-04/guix.png#right" | relative_url }})](https://www.gnu.org/software/guix/)

Happy birthday to [GNU Guix](https://www.gnu.org/software/guix/), which recently turned 10 years old! People have been sharing their stories, in which reproducible builds and bootstrappable builds are a recurring theme as a feature important to its users and developers. The experiences are [available on the GNU Guix blog](https://guix.gnu.org/en/blog/2022/10-years-of-stories-behind-guix/) as well as [a post on *fossandcrafts.org*](https://fossandcrafts.org/episodes/44-celebrating-a-decade-of-guix.html)

<br>

[![]({{ "/images/reports/2022-04/opensuse.png#right" | relative_url }})](https://www.opensuse.org/)

In [openSUSE](https://www.opensuse.org/), Bernhard M. Wiedemann posted his [usual monthly reproducible builds status report](https://lists.opensuse.org/archives/list/factory@lists.opensuse.org/thread/L36OUCW7PO4OWTDX2LGVCTRZ4O7WIXKT/).

<br>

## Upstream patches

The Reproducible Builds project detects, dissects and attempts to fix as many currently-unreproducible packages as possible. We endeavour to send all of our patches upstream where appropriate. This month, we wrote a large number of such patches, including:

* Bernhard M. Wiedemann:

    * [`python-numcodecs`](https://build.opensuse.org/request/show/972586) (CPU-related issue)
    * [`salt`](https://github.com/openSUSE/salt-packaging/pull/57) (date issue)
    * [`golang-github-prometheus-node_exporter`](https://github.com/prometheus/promu/pull/230) (date-related issue)
    * [`librsb`](https://bugzilla.opensuse.org/show_bug.cgi?id=1198822) (report compile-time CPU-detection)

* Chris Lamb:

    * [#1009981](https://bugs.debian.org/1009981) filed against [`rsync`](https://tracker.debian.org/pkg/rsync).
    * [#1010279](https://bugs.debian.org/1010279) filed against [`python-iso8601`](https://tracker.debian.org/pkg/python-iso8601).
    * [#1010316](https://bugs.debian.org/1010316) filed against [`python-cai`](https://tracker.debian.org/pkg/python-cai).
    * [#1010318](https://bugs.debian.org/1010318) filed against [`datalad`](https://tracker.debian.org/pkg/datalad).

* Johannes Schauer Marin Rodrigues:

    * [#1010368](https://bugs.debian.org/1010368) filed against [`python3.10`](https://tracker.debian.org/pkg/python3.10).

* Roland Clobus:

    * [#1009196](https://bugs.debian.org/1009196) filed against [`texlive-bin`](https://tracker.debian.org/pkg/texlive-bin).

* Vagrant Cascadian:

    * [#1009337](https://bugs.debian.org/1009337) filed against [`python-public`](https://tracker.debian.org/pkg/python-public).
    * [#1009339](https://bugs.debian.org/1009339) filed against [`python-gitlab`](https://tracker.debian.org/pkg/python-gitlab).
    * [#1009342](https://bugs.debian.org/1009342) filed against [`xfce4-panel-profiles`](https://tracker.debian.org/pkg/xfce4-panel-profiles).
    * [#1009796](https://bugs.debian.org/1009796) & [#1009797](https://bugs.debian.org/1009797) filed against [`apt`](https://tracker.debian.org/pkg/apt).
    * [#1009799](https://bugs.debian.org/1009799) filed against [`puppet`](https://tracker.debian.org/pkg/puppet).
    * [#1009931](https://bugs.debian.org/1009931) filed against [`gmp`](https://tracker.debian.org/pkg/gmp).
    * [#1009934](https://bugs.debian.org/1009934) filed against [`openssl`](https://tracker.debian.org/pkg/openssl).
    * [#1010043](https://bugs.debian.org/1010043) filed against [`serf`](https://tracker.debian.org/pkg/serf).
    * [#1010233](https://bugs.debian.org/1010233) filed against [`glibc`](https://tracker.debian.org/pkg/glibc).
    * [#1010238](https://bugs.debian.org/1010238) filed against [`binutils`](https://tracker.debian.org/pkg/binutils).
    * [#1010378](https://bugs.debian.org/1010378) filed against [`leds-alix`](https://tracker.debian.org/pkg/leds-alix).
    * [#1010379](https://bugs.debian.org/1010379) filed against [`snacc`](https://tracker.debian.org/pkg/snacc).
    * [#1010414](https://bugs.debian.org/1010414) filed against [`libcec`](https://tracker.debian.org/pkg/libcec).

<br>

## [diffoscope](https://diffoscope.org)

[![]({{ "/images/reports/2022-04/diffoscope.svg#right" | relative_url }})](https://diffoscope.org)

[*diffoscope*](https://diffoscope.org) is our in-depth and content-aware diff utility. Not only can it locate and diagnose reproducibility issues, it can provide human-readable diffs from many kinds of binary formats. This month, Chris Lamb prepared and uploaded versions [`210`](https://diffoscope.org/news/diffoscope-210-released/) and [`211`](https://diffoscope.org/news/diffoscope-211-released/) to Debian *unstable*, as well as noticed that some Python `.pyc` files are reported as `data`, so we should support `.pyc` as a fallback filename extension&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/e601024f)].

In addition, Mattia Rizzolo disabled the [Gnumeric](http://www.gnumeric.org/) tests in Debian as the package is not currently available&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/8ca65d71)] and dropped *mplayer* from `Build-Depends` too&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/ac595190)]. In addition, Mattia fixed an issue to ensure that the `PATH` environment variable is properly modified for all actions, not just when running the comparator.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/d06f3849)]

<br>

## Testing framework

[![]({{ "/images/reports/2022-04/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

The Reproducible Builds project runs a significant testing framework at [tests.reproducible-builds.org](https://tests.reproducible-builds.org), to check packages and other artifacts for reproducibility. This month, the following changes were made:

* Daniel Golle:

    * Prefer a different solution to avoid building all [OpenWrt](https://openwrt.org/) packages; skip packages from optional community feeds.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/aebf8e77)]

* Holger Levsen:

    * Detect Python deprecation warnings in the node health check.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/c8286cbc)]
    * Detect failure to build the Debian Installer.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/77f487f5)]

* Mattia Rizzolo:

    * Install *disorderfs* for building OpenWrt packages.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/50f140d7)]

* Paul Spooren ([OpenWrt](https://openwrt.org/)-related changes):

    * Don't build all packages whilst the core packages are not yet reproducible.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/d4610192)]
    * Add a missing `RUN` directive to `node_cleanup`.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/049f28b1)]
    * Be less verbose during a toolchain build.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/3c291f1b)]
    * Use disorderfs for rebuilds and update the documentation to match.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e173cde4)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/dc3cbec6)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/60849ab1)]

* Roland Clobus:

    * Publish the last reproducible Debian ISO image.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/caed5e8c)]
    * Use the `rebuild.sh` script from the `live-build` package.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b6cea0e1)]

Lastly, node maintenance was also performed by Holger Levsen [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/a2462544)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/2faced5e)].

---

If you are interested in contributing to the Reproducible Builds project, please visit our [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. However, you can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter: [@ReproBuilds](https://twitter.com/ReproBuilds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)
