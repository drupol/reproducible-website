---
layout: default
title: Contribute
permalink: /contribute/
order: 80
---

# Contribute

There are many ways to contribute to the Reproducible Builds project:

* **In the first instance, please join the [`rb-general` mailing-list](https://lists.reproducible-builds.org/listinfo/rb-general).**

* Our [Gitlab group](http://salsa.debian.org/reproducible-builds) is on [Salsa](https://salsa.debian.org/): submiting issues and opening pull requests is welcome! ([**Full signup instructions**]({{ "/contribute/salsa/" | relative_url }}))

* Subscribe to the [reproducible-builds@lists.alioth.debian.org mailing list](https://lists.alioth.debian.org/mailman/listinfo/reproducible-builds)
  and/or other [reproducible builds](https://lists.reproducible-builds.org/)
  oriented lists.

* Join the [#reproducible-builds IRC channel on irc.oftc.net](https://webchat.oftc.net/?channels=#reproducible-builds)
  and possibly [#reproducible-changes](https://webchat.oftc.net/?channels=#reproducible-changes)
  too. For Debian there is also [#debian-reproducible](https://webchat.oftc.net/?channels=#debian-reproducible)
  and [#debian-reproducible-changes](https://webchat.oftc.net/?channels=#debian-reproducible-changes),
  and for Arch Linux there is [#archlinux-reproducible on irc.libera.chat](https://web.libera.chat/?channels=#archlinux-reproducible)

You can also subscribe to [commit notifications](https://lists.reproducible-builds.org/listinfo/rb-commits)
to keep track of contributions.

### Reproducible Builds cares about all the distributions and projects out there

Reproducible Builds is distro agnostic, which means we care about all the distributions and projects out there.
Please send patches to this page if your project is not listed here.

## Contribute to distributions

Various distributions have efforts to become more reproducible:

* [Arch Linux]({{ "/contribute/archlinux/" | relative_url }} )
* [Debian]({{ "/contribute/debian/" | relative_url }})
* [GNU Guix]({{ "/contribute/guix/" | relative_url }})
* [NixOS]({{ "/contribute/nixos/" | relative_url }})

## Contribute to diffoscope, reprotest, disorderfs and strip-nondeterminism

{FIXME}

## Donate

Another way to help is to financially support our project. We welcome any
kind of donation, of any size. Please see our
[donation page]({{ "/donate/" | relative_url }}) for more information.
