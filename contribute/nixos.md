---
layout: default
title: Contribute to NixOS
permalink: /contribute/nixos/
---

Join the [#reproducible-builds:nixos.org](https://matrix.to/#/#reproducible-builds:nixos.org)
channel on matrix.

To rebuild a package locally and check it matches the one from the binary cache
you can use `nix-build '<nixpkgs>' -A notion --check`. This does not 'actively'
vary the environment like reprotest does, but will catch basic reproducibility
problems.

See [reproducible.nixos.org](https://reproducible.nixos.org) for the status of our current milestone:
building the nixos-unstable minimal and Gnome ISO's reproducibly. More information on the
problems listed there can be found on
[this pad](https://pad.sfconservancy.org/p/nixos-reproducible-builds-progress).

Look for [pull requests](https://github.com/NixOS/nixpkgs/pulls?q=is%3Aopen+is%3Apr+label%3A%226.topic%3A+reproducible+builds%22)
and [issues](https://github.com/NixOS/nixpkgs/issues?q=is%3Aopen+is%3Aissue+label%3A%226.topic%3A+reproducible+builds%22)
around reproducible builds.

Look into [trustix](https://tweag.github.io/trustix/).
